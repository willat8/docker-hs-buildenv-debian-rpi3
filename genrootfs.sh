#!/bin/bash
set -e

docker run --privileged -d -w /root --name ubuntu -it ubuntu:latest /bin/bash

docker exec --privileged ubuntu sh -c "apt-get update && apt-get install -y debootstrap qemu-user-static"

docker exec --privileged ubuntu qemu-debootstrap --arch=arm64 --verbose --variant=minbase stretch rootfs-debian-arm64 http://ftp.jp.debian.org/debian/

docker exec --privileged ubuntu tar --numeric-owner --create --gzip --file rootfs-debian-arm64.tar.gz --directory=rootfs-debian-arm64 .

docker cp ubuntu:/root/rootfs-debian-arm64.tar.gz .

