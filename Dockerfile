FROM scratch

ADD rootfs-debian-arm64.tar.gz /

RUN apt-get update && \
    apt-get upgrade -y

RUN apt-get install -y ghc

